Summary: Raw photo to FITS converter
Name: rawtran
Version: %(cat configure.ac | awk '{if(/AC_INIT/) {gsub("[\\[\\]\\,]"," ");  print $3;}}')
Release: 1
License: GPL2
Group: Applications/File
URL: http://integral.physics.muni.cz/%{name}
Source0: ftp://integral.physics.muni.cz/pub/%{name}/%{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
Requires: dcraw

%description
Rawtran is an utility to convert of raw photos in .CR2, .CRW, MRW, NEF,
RAF, etc. formats to the general astronomical FITS image format.
The  identification  of  raw photo type and decoding itself is done via
DCRAW utility by D.Coffin.

%prep
%setup -q

%build
%autoreconf -i -f
%configure CPPFLAGS=-I/usr/include/cfitsio -DNDEBUG -O4
make

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc %{_mandir}/man1/rawtran.1*
%{_bindir}/rawtran
%doc README COPYING NEWS
