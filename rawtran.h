/*

  rawtran
  Copyright (C) 2015-8  Filip Hroch, Masaryk University, Brno, CZ

  Rawtran is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Rawtran is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Rawtran.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <assert.h>

extern int rawtran(char *, char *, int, char *, char *, char *, char *,
		   char *,int);
extern char *darksh(char *filename);
extern char *darkfits(char *filename);

extern char *fitspath(const char *, const char *, int);
extern char *basename(const char *);
