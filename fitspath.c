/*

  fitsnames - manipulate with filenames
  Copyright (C) 2018-9  Filip Hroch, Masaryk University, Brno, CZ

  Rawtran is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Rawtran is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Rawtran.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "rawtran.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <assert.h>

#if defined _WIN32 || defined __CYGWIN__
#define DIRSEP '\\'
#else
#define DIRSEP '/'
#endif


char *fitspath(const char *raw, const char *suff, int clobber)
{
  const char *dot, *basename;
  char *fits;

  /* prepare string storage */
  fits = malloc(strlen(raw) + strlen(suff) + 2);
  assert(fits != NULL);

  /* overwrite flag */
  strcpy(fits, clobber ? "!" : "");

  /* locate basename */
  if( (basename = strrchr(raw,DIRSEP)) != NULL )
    basename++;
  else
    basename = raw;

  /* suffix like ".CR2" is replaced by ".fits" */
  if( (dot = strrchr(basename,'.')) != NULL )
    strncat(fits,basename,dot - basename);
  else
    strcat(fits,basename);

  /* add proper suffix */
  strcat(fits,suff);

  return(fits);
}

char *basename(const char *filename)
{
  const char *base;
  char *name;

  name = malloc(strlen(filename)+1);
  if( (base = strrchr(filename,DIRSEP)) != NULL )
    base++;
  else
    base = filename;
  strcpy(name,base);
  return(name);
}
