2019-01-21  Filip Hroch  <hroch@physics.muni.cz>

	* .hgtags:
	Added tag 1.1 for changeset c09bb9276a6a
	[14451a8252bf] [tip]

	* doc/rawtran.html:
	Updated docs.
	[c09bb9276a6a] [1.1]

	* doc/rawtran.html, rawtran.c:
	Minor update of html doc page.
	[f5c392f51f68]

2019-01-14  Filip Hroch  <hroch@physics.muni.cz>

	* README, configure.ac, dark.c, doc/IMG_5952_colouring.png,
	doc/IMG_5952_orig.png, doc/Makefile.am, doc/rawtran.html,
	fitspath.c, main.c, pixmap.c, rawtran.1, rawtran.c, rawtran.h:
	Large update (colour transformation is energy-based, docs):
	 * updated colour transformation matrixes
	 * the transformation is energy-based
	 * added description of quantity in the frame (photons, electron-
	volts).
	 * update (redesign, rewrote) of docs
	 * added section describing colour reconstruction
	 * minor optimalisation changes
	 * minor valgrind induced improvements
	[085bdfdc1694]

2018-11-04  Filip Hroch  <hroch@physics.muni.cz>

	* Makefile.am, configure.ac, dark.c, doc/rawtran.html, fitspath.c,
	main.c, pixmap.c, pixmap.h, rawtran.c, rawtran.h:
	Implemented threads processing and file path manipulations:

	* Rawtran main loop over filenames is implemented by threads.

	* Filenames processing removes absolute path from output names.
	Results are saved to the current directory.

	Both the improvements affects portability.
	[af2d49c32cf8]

2018-10-29  Filip Hroch  <hroch@physics.muni.cz>

	* configure.ac, doc/rawtran.html, main.c, pixmap.c, rawtran.1,
	rawtran.c, rawtran.h:
	Bugfix (options) and improvements:

	* Bugfix: the interchange of channels B and R in -c
	* Bugfix: I guess, the half of frame of -c plain was empty, possible
	similar fault of -c all has been checked
	* Optimised code for both speed and simplicity: direct conversion of
	all pixels to floats has been replaced by keeping of 16-bit unsigned
	integers where it is possible. Only filters conversions are
	internally computed in floats, results are rounded onto the
	integers. This enables does direct copy of large memory areas by
	memcpy() function, looping is necessary for pixels redistribution
	only.
	* Removed -B, --bitpix option, looks artificial now.
	* Renamed some -c switches (P to plain, u to clear, and s to scotopic)
	to improve readability of filters.
	* Multiple RAW frames can be passed.
	* By default, outputs are saved to files with *.fits suffix.
	* Added --no-clobber option and implemented overwrite/clobber
	behaviour like common system utilities (opposite to cfitsio
	conventions).
	* Updated code to give more clear error info.
	* Checking status of malloc(), it can fail for large frames.
	* Added signal handling to leave clean working directory, when
	interupted.
	* Extensive update of docs.
	[b065a8913190]

2018-07-22  Filip Hroch  <hroch@physics.muni.cz>

	* .hgtags:
	Added tag 1.0 for changeset 926b08218a5d
	[76204ffda8a5]

	* .hgtags:
	Removed tag 1.0
	[926b08218a5d] [1.0]

	* Makefile.am, blog.txt, configure.ac, doc/IMG_5952.CR2,
	doc/IMG_5952.png, doc/IMG_5952_B.png, doc/IMG_5952_Bi.png,
	doc/IMG_5952_Gi.png, doc/IMG_5952_R.png, doc/IMG_5952_Ri.png,
	doc/IMG_5952_V.png, doc/IMG_5952_X.png, doc/IMG_5952_Y.png,
	doc/IMG_5952_Z.png, doc/IMG_5952_s.png, doc/IMG_5952_u.png,
	doc/Makefile.am, doc/blog.txt, doc/rawtran.html,
	images/IMG_5952.CR2, images/IMG_5952.png, images/IMG_5952_B.png,
	images/IMG_5952_Bi.png, images/IMG_5952_Gi.png,
	images/IMG_5952_R.png, images/IMG_5952_Ri.png,
	images/IMG_5952_V.png, images/IMG_5952_X.png, images/IMG_5952_Y.png,
	images/IMG_5952_Z.png, images/IMG_5952_s.png, images/IMG_5952_u.png,
	images/Makefile.am, rawtran.1, rawtran.html:
	Updated docs.
	[16507b807da9]

	* .hgtags:
	Added tag 1.0 for changeset eb6df42b43d1
	[6ead3200bc64]

	* Makefile.am, configure.ac, rawtran.html, test/Makefile.am,
	test/fitslint.c, test/fitslint.sh:
	Removed test suite.
	[eb6df42b43d1]

	* .hgtags:
	Removed tag 1.0
	[408c204b4ec8]

	* .hgtags:
	Added tag 1.0 for changeset 18b54bcdc850
	[0ecba6ef51ae]

	* IMG_5952.CR2, INSTALL, Makefile.am, configure.ac, fitslint.c,
	fitslint.sh, images/IMG_5952.CR2, images/Makefile.am, main.c,
	pixmap.c, rawtran.1, rawtran.c, rawtran.h, rawtran.html,
	test/Makefile.am, test/fitslint.c, test/fitslint.sh:
	Updates. Cleanup.
	[18b54bcdc850]

2016-10-03  Filip Hroch  <hroch@physics.muni.cz>

	* main.c:
	Locale set to C to prevent non-english characters.
	[52039e500ee6]

2016-04-06  Filip Hroch  <hroch@physics.muni.cz>

	* .hgtags:
	Added tag 0.3.8 for changeset b1a3a7ba5758
	[6e8d8bf87fda]

	* configure.ac:
	Updated version number.
	[b1a3a7ba5758] [0.3.8]

	* .hgtags:
	Removed tag 0.3.8
	[cefb8c216b09]

	* .hgtags:
	Added tag 0.3.8 for changeset d633a90af621
	[32d43ac102dd]

	* dark.c, fitslint.c:
	Updated for gcc-5
	[d633a90af621]

2016-04-04  Filip Hroch  <hroch@physics.muni.cz>

	* .hgtags:
	Added tag 0.3.7 for changeset aabb9bfbd1f8
	[5b018b071b41]

	* IMG_5952.CR2, Makefile.am, configure.ac, fitslint.c, fitslint.sh,
	rawtran.html:
	Added check (test) suite.
	[aabb9bfbd1f8] [0.3.7]

2016-03-12  Filip Hroch  <hroch@physics.muni.cz>

	* .hgtags:
	Added tag 0.3.6 for changeset 920283dd7bf8
	[980e585dd8a8]

	* COPYING, configure.ac, rawtran.html:
	Updated obsolete COPYING (agains to *.c). Thx. O.Streicher.
	[920283dd7bf8] [0.3.6]

2016-03-10  Filip Hroch  <hroch@physics.muni.cz>

	* .hgtags:
	Added tag 0.3.5 for changeset eccf237049f2
	[4f7b8b97394e]

	* Makefile.am, README, configure.ac, images/Makefile.am:
	A documentation is part of installation process.
	[eccf237049f2] [0.3.5]

2016-03-09  Filip Hroch  <hroch@physics.muni.cz>

	* .hgtags:
	Added tag 0.3.4 for changeset 0a197eafa1e6
	[ab35573e4898]

	* debian/changelog, debian/compat, debian/control, debian/copyright,
	debian/rules, debian/source/format, rawtran.html:
	Removed debian subdirectory.
	[0a197eafa1e6] [0.3.4]

2015-04-06  Filip Hroch  <hroch@physics.muni.cz>

	* Makefile.am, dark.c, main.c, pixmap.c, pixmap.h, rawtran.1,
	rawtran.c, rawtran.h, rawtran.html:
	Conversion of PGM darks from FITS and PPM files.
	[29570a64bf8b]

2015-02-05  Filip Hroch  <hroch@physics.muni.cz>

	* main.c, rawtran.c, rawtran.h:
	Convenience function for dark. Docs comments: dark + invocation.
	[1b009d65eb11]

2015-02-03  Filip Hroch  <hroch@physics.muni.cz>

	* main.c, rawtran.c, rawtran.h:
	Comfortable handling of options.
	[0eefb8667c17]

2015-01-30  Filip Hroch  <hroch@physics.muni.cz>

	* Makefile.am, main.c, pixmap.c, pixmap.h, rawtran.c, rawtran.html:
	Code review (filters, types handlig, separation pixmaps,
	photosystem).
	[714135adecc7]

2015-01-29  Filip Hroch  <hroch@physics.muni.cz>

	* main.c, rawtran.c:
	Re-implemented plain mode - no Bayer de-masking. (by Nacho Mas)
	[736bb4fba88d]

2015-01-06  Filip Hroch  <hroch@physics.muni.cz>

	* Makefile.am, README, configure.ac, debian/changelog, debian/control,
	debian/copyright, debian/source/format, rawtran.1, rawtran.html,
	rawtran.spec:
	Debian packaging lint corrections.
	[f16d1eed86aa]

2014-04-18  Filip Hroch  <hroch@physics.muni.cz>

	* .hgtags:
	Added tag 0.3.3 for changeset 4b9391f4c5a6
	[a1d25cc5ba45]

	* debian/rules:
	Imported to Mercurial.
	[4b9391f4c5a6] [0.3.3]

2014-04-18  convert-repo  <convert-repo>

	* .hgtags:
	update tags
	[77dd9a553a54]

2014-04-18  hroch  <hroch>

	* INSTALL, bootstrap, configure.ac, debian/changelog,
	debian/changelog.template, debian/compat, debian/control,
	debian/rules, main.c, rawtran.1, rawtran.html, rawtran.spec:
	Debian-specific formal changes. Particular updates.
	[fcfa5b3c38b4]

2012-04-23  hroch  <hroch>

	* configure.ac, main.c, rawtran.c, rawtran.html:
	Removed type (-t) switch (updated command line interface and
	homepage).
	[85d9eea79011]

2012-04-06  hroch  <hroch>

	* bootstrap, main.c, rawtran.c, rawtran.html, rawtran.spec:
	Improved speed and simplified PPM loading.
	[9e083f4572d2]

2012-03-30  hroch  <hroch>

	* INSTALL, bootstrap, configure.ac, debian/changelog,
	debian/changelog.template, main.c, rawtran.c, rawtran.html,
	rawtran.spec:
	Implemented RPM packaging.
	[d9afc976db19]

2012-01-15  hroch  <hroch>

	* Makefile.am, bootstrap, rawtran.html:
	Updated: Homepage + bootstrap.
	[7f49006b150c]

2012-01-04  hroch  <hroch>

	* configure.ac, configure.in, debian/changelog, debian/control,
	debian/copyright, debian/rules, rawtran.c, rawtran.html:
	Added debian packager. Updated html page.
	[6b23c83adf4b]

2011-11-13  hroch  <hroch>

	* configure.in, main.c, rawtran.html:
	Updated documentation.
	[44823221db9f]

2011-07-31  hroch  <hroch>

	* rawtran.html:
	Updated design and install info in html page.
	[5b89b7a1be4a]

2011-07-28  hroch  <hroch>

	* rawtran.html:
	Reference on blog.
	[19f806d4d7fe]

2011-06-30  hroch  <hroch>

	* main.c, rawtran.html:
	Updated description.
	[2244f0041b65]

2011-06-11  hroch  <hroch>

	* blog.txt, rawtran.c, rawtran.html:
	Minor improvements.
	[d2fd4d0bdd12]

2011-05-01  hroch  <hroch>

	* blog.txt, rawtran.html:
	Improvements of documentation.
	[2117286275fd]

2011-04-30  hroch  <hroch>

	* rawtran.1, rawtran.c, rawtran.html:
	Keyword for colorspaces CSPACE. Rewoted man page. Enhanced html.
	[a017e454d3ef]

2011-04-24  hroch  <hroch>

	* main.c, rawtran.c, rawtran.html:
	More readable html page. Modified parameters and full instrumental
	as 3D array (use of FITS extensions abandoned).
	[eccc937e0ff6]

2011-04-08  hroch  <hroch>

	* rawtran.1, rawtran.c, rawtran.html:
	Instrumental replaced by Bayer. Docs update.
	[d8dcb9515b36]

2011-04-04  hroch  <hroch>

	* configure.in, main.c, rawtran.c, rawtran.html:
	Color image representation changed from band per HDU to 3D single
	HDU. Speed improvements. Updated version number.
	[e29e3272cd45]

2010-08-29  hroch  <hroch>

	* configure.in, images/IMG_5952_s.png, rawtran.html:
	Tiny enhancements.
	[bbbff83d858f]

2010-08-24  hroch  <hroch>

	* blog.txt, rawtran.c:
	The storage order of color bands reverted (XYZ -> ZYX).
	[33b5ea056355]

2010-04-05  hroch  <hroch>

	* rawtran.1, rawtran.html:
	Better description corrections.
	[893ff8aecba0]

	* main.c:
	An unknown parameter detection implemented.
	[7621267bbc18]

	* rawtran.c:
	Numerical range cut-off implemented.
	[8cb464e3c737]

2010-04-04  hroch  <hroch>

	* rawtran.html:
	Corrected html validation errors.
	[c78224a8774f]

	* rawtran.1, rawtran.html:
	Upgraded to finished html of fitspng.
	[b3eb04c52ea0]

2010-03-13  hroch  <hroch>

	* images/IMG_5952_B.png, images/IMG_5952_R.png, images/IMG_5952_V.png,
	images/IMG_5952_s.png, rawtran.c, rawtran.html:
	Transformation updated to D65. Corrected scotopic transformation.
	[ff86e4599c65]

2010-03-12  hroch  <hroch>

	* images/IMG_5952.png, images/IMG_5952_B.png, images/IMG_5952_Bi.png,
	images/IMG_5952_Gi.png, images/IMG_5952_R.png,
	images/IMG_5952_Ri.png, images/IMG_5952_V.png,
	images/IMG_5952_X.png, images/IMG_5952_Y.png, images/IMG_5952_Z.png,
	images/IMG_5952_s.png, images/IMG_5952_u.png, rawtran.1, rawtran.c,
	rawtran.html:
	Updated webpage.
	[b83b6f9a7de3]

	* rawtran.1:
	Output file correction.
	[b7a1c8806ef7]

	* rawtran.html:
	Web page founded.
	[b0241bd4447f]

2010-03-11  hroch  <hroch>

	* main.c, rawtran.1, rawtran.c:
	Added scotopic vision.
	[070946150efc]

	* bootstrap, configure.in, main.c:
	Improvements in version designation.
	[d035daceb507]

2010-03-10  hroch  <hroch>

	* rawtran.1, rawtran.c:
	Implemented BVR transfarmations.
	[654e5f7d06bf]

2010-03-07  hroch  <hroch>

	* main.c, rawtran.1, rawtran.c:
	Implemented exact usage of color spaces XYZ and BVR and a new
	selection of instrumental data.
	[bd3e372ee21b]

2009-09-20  hroch  <hroch>

	* rawtran.c:
	Updated color info for channels FILTER, comment and related ones.
	[f3a02028e826]

	* configure.in:
	Increased version.
	[fe137e2607bd]

2009-09-18  hroch  <hroch>

	* rawtran.c:
	Fine tuning of color conversion parameters.
	[04efb5472a88]

	* rawtran.1:
	Reformulation.
	[c7eaff221ed7]

2009-08-26  hroch  <hroch>

	* rawtran.1:
	Color related update.
	[240a05b92a82]

	* main.c:
	Update License.
	[f9f2ea0b8c96]

	* rawtran.c:
	Implemented color pixmap and more correct handling of colors in RAW.
	[11d646cf0898]

2009-08-18  hroch  <hroch>

	* rawtran.c:
	Specified better color space flag.
	[ba674a3284b2]

2009-08-17  hroch  <hroch>

	* rawtran.c:
	Bugfix. Camera multipliers replaced by Daylight multipliers which
	more closely reproduces colors.
	[4c1aace27549]

2009-03-28  hroch  <hroch>

	* blog.txt:
	Recent improvements described.
	[5be7733813ef]

	* rawtran.c:
	Implemented type 4. Header keywords for color images changed.
	Cleanup.
	[d34004c6673b]

	* main.c:
	Implemented type 4. Cleanup.
	[3e9a5a06d2cb]

	* configure.in:
	Removed library path search.
	[cf3a20ef36f4]

	* rawtran.1:
	Decribed types of output images, a new type 4. Enhancements.
	[50edb0f9e6bd]

2009-03-15  hroch  <hroch>

	* main.c:
	Updated.
	[9d0224554d69]

	* main.c:
	Implemented run-time detection of dcraw, optional switches to dcraw.
	Updated on-line help.
	[679c0f4d4636]

	* rawtran.c:
	Implemented optional paramaters to dcraw. array is now inline and
	without range checking.
	[7c0878f275cf]

	* rawtran.1:
	Updated to -X and dcraw detection. Better description of -t.
	[fef7a303ddf9]

2009-01-19  hroch  <hroch>

	* rawtran.1:
	Fine tuning of terms by notes about raw photos by D.Coffin (see end
	of dcraw hoempage).
	[d0bda70e7efc]

2008-12-21  hroch  <hroch>

	* blog.txt:
	Documentation founded.
	[754175e1dcb4]

2008-12-19  hroch  <hroch>

	* rawtran.c:
	Many changes: new options, white balance, another format of fitses.
	Bugfixes in color mask.
	[8424ea2ac22d]

	* rawtran.1:
	New options. Better description.
	[259eed0e2be9]

	* main.c:
	New options. Small arrangements.
	[6260b7d3d7a6]

2008-11-16  hroch  <hroch>

	* rawtran.1:
	Some small stylistic corrections.
	[c44abbdf53b3]

2008-02-14  hroch  <hroch>

	* rawtran.c:
	Bugfix of focal lenght. Added CVS Id to comments.
	[6bb0363e4a47]

2008-02-13  hroch  <hroch>

	* rawtran.c:
	Implemented rotation (problem with rotated images and dark
	subtraction solved).
	[808c2c512d61]

2008-01-05  hroch  <hroch>

	* AUTHORS, COPYING, ChangeLog, INSTALL, Makefile.am, NEWS, README,
	bootstrap, configure.in, main.c, rawtran.1, rawtran.c:
	Initial revision
	[8fbfd7a82e7c]

